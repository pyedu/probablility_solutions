import itertools
from itertools import count

from itertoolz import *
from toolz import functoolz as ft
import toolz as tz

import numpy as np


def iprint(iterable):
    print(list(iterable))


def throw(n):
    while True:
        array = np.random.choice(6, n, replace=True) + 1
        # print(array)
        yield array

def count_odd_6(array):
    odd = 0
    six = 0
    for i in array:
        if i == 6:
            six += 1
        elif i % 2 == 1:
            odd += 1
    # print(odd, six)
    return odd, six, six * odd

print(help(take))
iprint(take(9, throw(6)))

sum_of_prod = 0
n = 99999
res = sum([count_odd_6(array)[2] for array in take(n, throw(6))])
print(res)
print(res / n)

