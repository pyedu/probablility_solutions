import itertools
from itertools import count
from math import comb
from numbers import Rational

from itertoolz import *
from toolz import functoolz as ft
import toolz as tz

import numpy as np


def iprint(iterable):
    print(list(iterable))


def full_result(N=6):
    return sum(partial_sum(n, N) for n in range(N+1))


def partial_sum(n, N):
    return sum(item(n, m, N) for m in range(N - n + 1))


def item(n, m, N):
    p_nm = comb(N, n+m) * comb(n+m, n) / (6**n * 2**m * 3 ** (N - n - m))
    return n * m * p_nm


print(f"{full_result()=}")


def fact(k):
    if k == 0:
        return 1
    return k * fact(k-1)


def result_0():
    return sum(subsum_0(n) for n in range(7))

def subsum_0(n):
    # return sum((fact(6) / (fact(n)* fact(m) *fact(6-m-n)) * .5**6 * (1/3)**(6-m) for n in range(6-m + 1)))  # For getting one in a difficult way :-)
    return sum(n*m*fact(6) / (fact(n) * fact(m) * fact(6-m-n)) * (1/6)**n * .5**m * (1/3)**(6-m-n) for m in range(6-n + 1))

print(f"{result_0()=}")


def probsum_3():  # Should be 1
    """Sum of the probabilities"""
    return fact(6) / 3**6 * sum((3/2)**m / fact(m) * subsum_3(6 - m) for m in range(7))

def subsum_3(k):
    return sum(.5**n / (fact(n)*fact(k-n)) for n in range(k + 1))
print(f"{probsum_3()=}   Should be 1.")


def result_4():
    return fact(6) / 3**6 * sum((3/2)**m * m / fact(m) * subsum_4(6 - m) for m in range(7))

def subsum_4(k):
    return sum(n * .5**n / (fact(n)*fact(k-n)) for n in range(k + 1))

subsum_4s = [subsum_4(k) for k in range(6 + 1)]
print([s.as_integer_ratio() for s in subsum_4s])

print(f"{subsum_4s=}")

print(f"{result_4()=}")



def partial_probability_average(num_six):
    prob_of_six = comb(6, num_six) * 5 ** num_six / (6 ** 6)
    odd_p = 1 / 2
    odd_n = 6 - num_six
    expect_odd = odd_n * odd_p
    return prob_of_six * num_six * expect_odd


def partial_probability_detailed(num_six):
    prob_of_six = comb(6, num_six) * 5 ** num_six / (6 ** 6)

    odd_max_n = 6 - num_six
    sum_partial = 0
    for num_odd in range(0, odd_max_n + 1):
        prob_odd = comb(odd_max_n, num_odd) * .5 ** odd_max_n
        sum_partial += prob_of_six * prob_odd * num_odd * num_six
    return sum_partial


def full_result_wrong(start=0, stop=7, partial_probability=partial_probability_detailed):
    summ = 0
    for i in range(start, stop):
        pp = partial_probability(i)
        print(i, pp)
        summ += pp
    return summ


# print(full_probability_wrong())
# print(full_probability_wrong(1, 6))
