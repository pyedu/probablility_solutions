Works with Python 3.9. For older versions remove = from f"{var=}" f-strings first. That may solve your problems.

Részben a Rényi et al féle Valszám feladatgyűjtemény megoldásai, szimulációi

- functional_6_throw* covariancia számítás szimulációja és kiszámítási kísérlete
   6 dobásból 6-osok száma és a páratlanok száma közötti korreláció kiszámításánál
   az E(XY) kiszámítási kísérlete

